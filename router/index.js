const http = require ("http");
const express = require ("express"); 
//objeto ruta dentro de express
const router = express.Router();
const bodyParser = require("body-parser");

router.get("/formulario", (req, res) => {
	const params = {
		valor: req.body.valor,
		destino: req.body.destino,
		nombre: req.body.nombre,
		anos: req.body.anos,
		tipoViaje: req.body.tipoViaje,
		precio: req.body.precio,
		subtotal: req.body.subtotal,
		impuesto: req.body.impuesto,
		descuento: req.body.descuento,
		total: req.body.total,
		plazo: req.body.plazo
	};
	res.render("formulario.html", params);
});

router.post("/formulario", (req, res) => {
	const params = {
		valor: req.body.valor,
		destino: req.body.destino,
		nombre: req.body.nombre,
		anos: req.body.anos,
		tipoViaje: req.body.tipoViaje,
		precio: req.body.precio,
		subtotal: req.body.subtotal,
		impuesto: req.body.impuesto,
		descuento: req.body.descuento,
		total: req.body.total,
		plazo: req.body.plazo
	};
	res.render("formulario.html", params);
});


//PARTE FINAL: importar o publicar rutas para que app pueda manejar el archivo de rutas
module.exports = router;