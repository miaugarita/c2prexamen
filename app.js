const http = require("http");
const express = require("express");
const bodyparser = require("body-parser");

const misRutas = require("./router/index.js");
const path = require("path");


const app = express();
app.set("view engine", "ejs");
//tomar valor estatico : carpet ay se agrega public
app.use(express.static(__dirname + "/public"));
app.use(bodyparser.urlencoded({ extended: true }));


//cambiar extenciones ejs a html
app.engine("html", require("ejs").renderFile);
//USAR RUTAS
app.use(misRutas);



//LA PAGINA ERROR VA AL FINAL DE GET/ POST
app.use((req, res, next) => {
	res.status(404).sendFile("/views/404error.html");
});

const puerto = 500;
app.listen(puerto, () => {
	console.log("Iniciando puerto");
});